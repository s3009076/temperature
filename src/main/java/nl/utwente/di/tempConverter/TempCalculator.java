package nl.utwente.di.tempConverter;

/**
 * Toy calculator that converts Celsius to Fahrenheit.
 */
public class TempCalculator {
    public static double convert(double celsius) {
        return 1.8 * celsius + 32;
    }
}
