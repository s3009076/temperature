package nl.utwente.di.tempConverter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Toy Servlet that converts an inputted Celsius temperature to Fahrenheit.
 */
public class TempConverter extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        // funny html string concatenation
        String title = "Temperature Conversion";
        String html = "";
        try {
            Double temp = Double.valueOf(request.getParameter("temp"));
            html =
                "<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Temperature (Celsius): " +
                    request.getParameter("temp") + "\n" +
                    "  <P>Temperature (Fahrenheit): " +
                    TempCalculator.convert(temp) +
                    "</BODY></HTML>";
        } catch (NumberFormatException e) {
            System.err.println("oops! should notify the user!");
            html =
                "<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>That was not a valid temperature! " +
                    "</BODY></HTML>";
        } finally {
            out.println(html);
        }
    }
}
